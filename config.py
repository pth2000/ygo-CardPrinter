import configparser
import os


class Config:
    def __init__(self):
        # 页边距
        self.top_margin = 10
        self.bottom_margin = 10
        self.left_margin = 10
        self.right_margin = 10
        # 纸张大小
        self.page_height = 297
        self.page_width = 210
        # 卡片大小
        self.card_height = 86
        self.card_weight = 59
        # 表格自动调整尺寸
        self.cell_autofit = True
        # 单元格边距
        self.cell_margins_start = 0
        self.cell_margins_end = 0
        self.cell_margins_top = 0
        self.cell_margins_bottom = 0

    def load(self):
        """载入配置文件"""
        if not os.path.exists('setting.ini'):
            self.init_save()
            return

        config_parser = configparser.ConfigParser()
        config_parser.read('setting.ini')

        self.top_margin = float(config_parser.get('section', 'top_margin'))
        self.bottom_margin = float(config_parser.get('section', 'bottom_margin'))
        self.left_margin = float(config_parser.get('section', 'left_margin'))
        self.right_margin = float(config_parser.get('section', 'right_margin'))

        self.page_height = float(config_parser.get('section_page', 'page_height'))
        self.page_width = float(config_parser.get('section_page', 'page_width'))

        self.card_height = float(config_parser.get('card_size', 'card_height'))
        self.card_weight = float(config_parser.get('card_size', 'card_weight'))

        self.cell_autofit = True if config_parser.get('cell_margins', 'cell_autofit') == 'True' else False
        self.cell_margins_start = float(config_parser.get('cell_margins', 'cell_margins_start'))
        self.cell_margins_end = float(config_parser.get('cell_margins', 'cell_margins_end'))
        self.cell_margins_top = float(config_parser.get('cell_margins', 'cell_margins_top'))
        self.cell_margins_bottom = float(config_parser.get('cell_margins', 'cell_margins_bottom'))

    def init_save(self):
        """生成默认配置文件"""
        config_parser = configparser.ConfigParser()
        config_parser.add_section('section')
        config_parser.set('section', 'top_margin', str(self.top_margin))
        config_parser.set('section', 'bottom_margin', str(self.bottom_margin))
        config_parser.set('section', 'left_margin', str(self.left_margin))
        config_parser.set('section', 'right_margin', str(self.right_margin))

        config_parser.add_section('section_page')
        config_parser.set('section_page', 'page_height', str(self.page_height))
        config_parser.set('section_page', 'page_width', str(self.page_width))

        config_parser.add_section('card_size')
        config_parser.set('card_size', 'card_height', str(self.card_height))
        config_parser.set('card_size', 'card_weight', str(self.card_weight))

        config_parser.add_section('cell_margins')
        config_parser.set('cell_margins', 'cell_autofit', str(self.cell_autofit))
        config_parser.set('cell_margins', 'cell_margins_start', str(self.cell_margins_start))
        config_parser.set('cell_margins', 'cell_margins_end', str(self.cell_margins_end))
        config_parser.set('cell_margins', 'cell_margins_top', str(self.cell_margins_top))
        config_parser.set('cell_margins', 'cell_margins_bottom', str(self.cell_margins_bottom))
        config_parser.write(open('setting.ini', 'w'))


config = Config()
