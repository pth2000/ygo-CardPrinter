# ygo-CardPrinter ygo卡图批量打印工具

## 介绍
此程序可将一个文件夹下的所有卡图以3x3的形式在word中排版，可以用于快速打印卡图。

[点击进入发行版界面下载](https://gitee.com/pth2000/ygo-CardPrinter/releases)

在发行版界面中获取编译后的对应各操作系统的可执行文件。

![Image](/images/截图.png)


## 使用说明
### 配置（非必要）

支持自定义打印样式，使用文本编辑器打开 setting.ini 文件，修改对应值即可。

|      节       |          键          |    值类型     |    释义    |
|:------------:|:-------------------:|:----------:|:--------:|
|   section    |     top_margin      | float (mm) |  页边距-顶   |
|   section    |    bottom_margin    | float (mm) |  页边距-底   |
|   section    |     left_margin     | float (mm) |  页边距-左   |
|   section    |    right_margin     | float (mm) |  页边距-右   |
| section_page |     page_height     | float (mm) |  纸张大小-高  |
| section_page |     page_width      | float (mm) |  纸张大小-宽  |
|  card_size   |     card_height     | float (mm) |  卡片大小-高  |
|  card_size   |     card_weight     | float (mm) |  卡片大小-宽  |
| cell_margins |    cell_autofit     |  boolean   | 单元格-自动布局 |
| cell_margins | cell_margins_start  | float (mm) | 单元格间距-始  |
| cell_margins |  cell_margins_end   | float (mm) | 单元格间距-末  |
| cell_margins |  cell_margins_top   | float (mm) | 单元格间距-顶  |
| cell_margins | cell_margins_bottom | float (mm) | 单元格间距-底  |

**注意：仅 cell_autofit 的值为 False 时，单元格间距的调整才生效。**

默认的打印样式为：
- 纸张大小：A4 (297mm*210mm)
- 页边距：10mm
- 卡片尺寸：86mm*59mm
- 单元格间距：自动


### 运行
1. 运行程序，选择图片所在文件夹路径，程序将会遍历文件夹下的所有图片文件（jpg/png）。
2. 结束运行后，将在程序同目录下生成output.docx文件。
